# File Util #

 Allow process directory with given command.

#Usage

Run in Command Prompt with params:

 [start up directory] [cmd] [output directory]*

Example:

 C:/Documents all C:/Documents/output.txt

#Allowed commands:
- all (remeber path from start up directory)
- cpp (remeber path from start up directory for files with .cpp extension and concat with " /"
- reverse1 (remeber path from start up directory and reverse file names)
- reverse 2(remebr path from start up directory and reverse string with file name)