﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaytikaTestTask.commands
{
    /// <summary>
    /// Resolve command name with command executor
    /// </summary>
    public class CommandFactory
    {
        private const string ALL_CMD = "all";
        private const string CPP_CMD = "cpp";
        private const string REVERSE_1_CMD = "reverse1";
        private const string REVERSE_2_CMD = "reverse2";

        public static IFileProcessorCommand Resolve(string cmdName)
        {
            if(cmdName == ALL_CMD)
            {
                return new AllFileProccessorCommand();
            }

            if(cmdName == CPP_CMD)
            {
                return new CppFileProccessorCommand();
            }

            if(cmdName == REVERSE_1_CMD)
            {
                return new ReverseOneFileProccessorCommand();
            }

            if(cmdName == REVERSE_2_CMD)
            {
                return new ReverseTwoFileProccessorCommand();
            }

            throw new NotImplementedException(string.Format("Unknown command {0}", cmdName));
        }
    }
}
