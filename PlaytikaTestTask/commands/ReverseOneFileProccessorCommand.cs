﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PlaytikaTestTask.models;

namespace PlaytikaTestTask.commands
{
    /// <summary>
    /// Save path to file from start up dir and reverse path
    /// </summary>
    public class ReverseOneFileProccessorCommand : IFileProcessorCommand
    {
        public string Execute(FileProcessorInput input)
        { 
            var localPath = input.fileName.Replace(input.startUpDir, "");
            var subPathes = localPath.Split(Path.DirectorySeparatorChar);
            var reversePathes = subPathes.Reverse();

            return string.Join(Path.DirectorySeparatorChar.ToString(), reversePathes.ToArray());
        }
    }
}
