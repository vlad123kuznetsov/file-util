﻿using PlaytikaTestTask.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaytikaTestTask.commands
{
    /// <summary>
    /// Save path to file from start up dir and reverse string
    /// </summary>
    public class ReverseTwoFileProccessorCommand : IFileProcessorCommand
    {
        public string Execute(FileProcessorInput input)
        {
            var localPath = input.fileName.Replace(input.startUpDir, "");
            return Reverse(localPath);
        }

        private string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
