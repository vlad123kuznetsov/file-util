﻿using PlaytikaTestTask.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PlaytikaTestTask.commands
{
    /// <summary>
    /// Save path to file from start up directory for each file with extnesion .cpp and add /
    /// </summary>
    public class CppFileProccessorCommand : IFileProcessorCommand
    {
        private const string CPP_EXT = ".cpp";

        public string Execute(FileProcessorInput input)
        {
            var fileInfo = new FileInfo(input.fileName);

            if (fileInfo.Extension == CPP_EXT)
            {
                return input.fileName.Replace(input.startUpDir, "") + " /";
            }

            return input.fileName;
        }
    }
}
