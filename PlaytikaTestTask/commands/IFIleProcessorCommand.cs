﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlaytikaTestTask.models;

namespace PlaytikaTestTask.commands
{
    public interface IFileProcessorCommand
    {
        string Execute(FileProcessorInput fileName);
    }
}
