﻿using PlaytikaTestTask.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaytikaTestTask.commands
{
    /// <summary>
    /// Save path to file from start up directory
    /// </summary>
    public class AllFileProccessorCommand : IFileProcessorCommand
    {
        public string Execute(FileProcessorInput input)
        {
            return input.fileName.Replace(input.startUpDir, "");
        }
    }
}
