﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaytikaTestTask.models
{
    public class FileProcessorInput
    {
        public readonly string startUpDir;
        public readonly string fileName;

        public FileProcessorInput(string _fileName, string _startUpDir)
        {
            startUpDir = _startUpDir;
            fileName = _fileName;
        }
    }
}
