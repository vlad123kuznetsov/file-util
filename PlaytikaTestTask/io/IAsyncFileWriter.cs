﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaytikaTestTask.io
{
    public interface IAsyncFileWriter
    {
        Task Write(string path, IEnumerable<string> data);
    }
}
