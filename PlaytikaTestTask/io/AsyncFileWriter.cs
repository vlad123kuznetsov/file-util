﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaytikaTestTask.io
{
    /// <summary>
    /// Write enumerable to file
    /// </summary>
    public class AsyncFileWriter : IAsyncFileWriter
    {
        async Task IAsyncFileWriter.Write(string path, IEnumerable<string> data)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (var item in data)
                {
                    await writer.WriteLineAsync(item);
                }
            }
        }
    }
}
