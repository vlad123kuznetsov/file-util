﻿using Autofac;
using PlaytikaTestTask.commands;
using PlaytikaTestTask.io;
using PlaytikaTestTask.models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlaytikaTestTask
{
    /// <summary>
    /// Procces files in given directory with given command and output result to file
    /// </summary>
    public class FileCommandLineUtil : ICommandLineUtil
    {
        private const string DEFAULT_OUTPUT = "results.txt";

        private string cmd = "";
        private string startUpDir = "";
        private string outputFileName = "";
        private IContainer Container;
        private ConcurrentBag<Task> tasks = new ConcurrentBag<Task>();
        private ConcurrentQueue<string> processedData = new ConcurrentQueue<string>();

        public async Task RunUtil(string[] args, IContainer _container)
        {
            try
            {
                Container = _container;

                startUpDir = args[0];
                cmd = args[1];

                if (args.Length == 2)
                {
                    outputFileName = Path.Combine(Environment.CurrentDirectory, DEFAULT_OUTPUT);
                }
                else
                {
                    outputFileName = args[2];
                }

                tasks.Add(Task.Run(() => ProcessDir(startUpDir)));

                await Task.WhenAll(tasks.ToArray());

                Console.WriteLine("Processed " + tasks.Count + " files");

                await WriteResultToFile(outputFileName);

                Console.WriteLine("Written to file " + outputFileName);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error occus : " + e.Message);
            }
        }

        /// <summary>
        /// Write util result to file
        /// </summary>
        /// <param name="outPutPath"></param>
        /// <returns></returns>
        private async Task WriteResultToFile(string outPutPath)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var writer = scope.Resolve<IAsyncFileWriter>();
                await writer.Write(outPutPath, processedData);
            }
        }

        /// <summary>
        /// Search to files and another directories in given directory
        /// </summary>
        /// <param name="dirPath"></param>
        private void ProcessDir(string dirPath)
        {
           var dirInfo = new DirectoryInfo(dirPath);
           foreach (var directoryName in dirInfo.GetDirectories())
           {
               tasks.Add(Task.Run(() => ProcessDir(directoryName.FullName)));
           }

           foreach (var fileName in dirInfo.GetFiles())
           {
               tasks.Add(Task.Run(() => ProcessFileName(fileName.FullName)));
           }
        }

        /// <summary>
        /// Execute command with given file
        /// </summary>
        /// <param name="fileName"></param>
        private void ProcessFileName(string fileName)
        {
            var cmdRunner = CommandFactory.Resolve(cmd);
            var fileInput = new FileProcessorInput(fileName, startUpDir);
            var cmdResult = cmdRunner.Execute(fileInput);
            processedData.Enqueue(cmdResult);
        }

        /// <summary>
        /// Check that passed params is valid
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public bool InputIsValid(string[] args)
        {
            var validCommands = new List<string>() { "all", "cpp", "reverse1", "reverse2" };
            var template = "{dir} {cmd} {outputdir}";
            if (args.Length > 3 || args.Length < 2)
            {
                Console.WriteLine("Invalid params. Should follow template: " + template);
                return false;
            }

            startUpDir = args[0];
            if (!Directory.Exists(startUpDir))
            {
                Console.WriteLine("Directory " + args[0] + " not exist");
                return false;
            }

            if (validCommands.IndexOf(args[1]) == -1)
            {
                Console.WriteLine("Unrecognized command " + args[1]);
                return false;
            }

            return true;
        }
    }
}
