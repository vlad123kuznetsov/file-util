﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlaytikaTestTask.io;
using PlaytikaTestTask.commands;
using System.IO;
using Autofac;
using System.Collections.Concurrent;
using PlaytikaTestTask.models;
using System.Threading;

namespace PlaytikaTestTask
{
    public class PlaytikaFileUtil
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<AsyncFileWriter>().As<IAsyncFileWriter>();
            builder.RegisterType<FileCommandLineUtil>().As<ICommandLineUtil>();

            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope())
            {
                var utilProcessor = scope.Resolve<ICommandLineUtil>();
                if(utilProcessor.InputIsValid(args))
                {
                    Task.Run(() => utilProcessor.RunUtil(args, container), default(System.Threading.CancellationToken));
                }

                Console.ReadKey();
            }
        }
    }
}
