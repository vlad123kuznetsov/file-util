﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaytikaTestTask
{
    public interface ICommandLineUtil
    {
        bool InputIsValid(string[] args);
        Task RunUtil(string[] args, IContainer container);
    }
}
