﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Xunit;
using PlaytikaTestTask;
using Autofac;
using PlaytikaTestTask.io;

namespace PlatikaTestTask.Tests
{
    public class TestUtil
    {
        /// <summary>
        /// Util must run with correct input
        /// </summary>
        /// <param name="path"></param>
        /// <param name="cmd"></param>
        /// <param name="output"></param>
        [Theory]
        [InlineData(@"C:\Users\user\Documents\", "cpp", @"C:\Users\user\Documents\")]
        [InlineData(@"C:\Users\user\Documents\", "all", @"C:\Users\user\Documents\")]
        [InlineData(@"C:\Users\user\Documents\", "reverse1", @"C:\Users\user\Documents\")]
        [InlineData(@"C:\Users\user\Documents\", "reverse2", @"C:\Users\user\Documents\")]
        public void TestInputValid(string path, string cmd, string output)
        {
            var utilLogic = new FileCommandLineUtil();
            Assert.Equal(utilLogic.InputIsValid(new string[] { path, cmd, output }), true);
        }

        /// <summary>
        /// Util can't run with incorrect input
        /// </summary>
        /// <param name="path"></param>
        /// <param name="cmd"></param>
        /// <param name="output"></param>
        [Theory]
        [InlineData(@"C:\Users\user\Missing\", "cpp", @"C:\Users\user\Documents\")]
        [InlineData(@"C:\Users\user\Documents\", "reverse3", @"C:\Users\user\lol\")]
        public void TestInputInvalid(string path, string cmd, string output)
        {
            var utilLogic = new FileCommandLineUtil();
            Assert.Equal(utilLogic.InputIsValid(new string[] { path, cmd, output }), false);
        }

        /// <summary>
        /// Check that output file created after running util
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData(@"C:\Users\user\Documents\Test1099\", "cpp", @"C:\Users\user\Documents\Test1099\output")]
        public async Task TestUtilValid(string fileStartUpDir, string cmd, string output)
        {
            var utilLogic = new FileCommandLineUtil();

            //prepare test data

            if (!Directory.Exists(fileStartUpDir))
            {
                Directory.CreateDirectory(fileStartUpDir);
            }
            else
            {
                var files = Directory.GetFiles(fileStartUpDir);
                foreach (var item in files)
                {
                    File.Delete(item);
                }
            }

            var fileNames = new string[]
            {
                "1.cpp",
                "1.cpp",
                "3.cpp",
                "4.txt",
                "4.mm"
            };

            var content = new string[]
            {
                "some content"
            };

            foreach (var file in fileNames)
            {
                using (var str = new StreamWriter(fileStartUpDir + file))
                {
                    str.WriteLine(content);
                }
            }

            var builder = new ContainerBuilder();

            builder.RegisterType<AsyncFileWriter>().As<IAsyncFileWriter>();
            builder.RegisterType<FileCommandLineUtil>().As<ICommandLineUtil>();

            var container = builder.Build();

            await utilLogic.RunUtil(new string[] { fileStartUpDir, cmd, output }, container);

            Assert.Equal(File.Exists(output), true);
        }
    }
}
