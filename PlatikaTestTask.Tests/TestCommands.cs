﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlaytikaTestTask.commands;
using Xunit;
using PlaytikaTestTask.models;

namespace PlatikaTestTask.Tests
{
    public class TestCommands
    {
        /// <summary>
        /// Check all command with correct input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\somefolder\somedata", @"somefolder\somedata", @"C:\")]
        public void AllValid(string data,  string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new AllFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.Equal(result, shoudBe);
        }

        /// <summary>
        /// Check all command with invalid input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\somefolder\somedata", @"C:\somefolder\somedata", @"C:\")]
        public void AllInvalid(string data, string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new AllFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.NotEqual(result, shoudBe);
        }

        /// <summary>
        /// Check cpp command with valid input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\somefolder\somedata\1.cpp", @"somefolder\somedata\1.cpp /", @"C:\")]
        public void CppValid(string data, string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new CppFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.Equal(result, shoudBe);
        }

        /// <summary>
        /// Check cpp with invalid input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\somefolder\somedata", @"somefolder\somedata", @"C:\")]
        public void CppInvalid(string data, string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new CppFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.NotEqual(result, shoudBe);
        }

        /// <summary>
        /// Check reverse with valid input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\somefolder\somedata", @"somedata\somefolder", @"C:\")]
        public void ReverseOneValid(string data, string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new ReverseOneFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.Equal(result, shoudBe);
        }

        /// <summary>
        /// Check reverse one with invalid input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\somefolder\somedata", @"somefolder\somedata", @"C:\")]
        public void ReverseOneInvalid(string data, string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new ReverseOneFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.NotEqual(result, shoudBe);
        }

        /// <summary>
        /// Check reverse two with valid input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\some", @"emos", @"C:\")]
        public void ReverseTwoValid(string data, string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new ReverseTwoFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.Equal(result, shoudBe);
        }

        /// <summary>
        /// Check reverse two with invalid input
        /// </summary>
        /// <param name="data"></param>
        /// <param name="shoudBe"></param>
        /// <param name="launchPath"></param>
        [Theory]
        [InlineData(@"C:\somefolder\somedata", @"somefolder\somedata", @"C:\")]
        public void ReverseTwoInvalid(string data, string shoudBe, string launchPath)
        {
            var fileProcessorCommand = new FileProcessorInput(data, launchPath);
            var result = new ReverseTwoFileProccessorCommand().Execute(fileProcessorCommand);
            Assert.NotEqual(result, shoudBe);
        }
    }
}
